import Vue from 'vue'
import App from './App.vue'
import texts from './constants'
import message from './utils'

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')

message(texts.text1)